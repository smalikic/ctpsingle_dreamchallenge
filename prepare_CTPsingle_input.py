import sys;
import os;
import getopt;
import shutil;


class CNA(object):
	chromosome=""
	start_pos=0
	end_pos=0
	BAF=0
	major_allele=0
	minor_allele=0
	frac=0.0

nuc_to_number = {};
nuc_to_number["A"] = 0;
nuc_to_number["C"] = 1;
nuc_to_number["G"] = 2;
nuc_to_number["T"] = 3;


gender = "unknown";
SNV_position_coverage_cutoff = 20;
genome_length = 3*1000*1000*1000;

#Use neutral + unknown for CTPsingle input 
def SNV_copy_number_status(chromosome, SNV_position, CNV_array):

	for CNA_event in CNV_array: 
		if SNV_position >= CNA_event.start_pos and SNV_position <= CNA_event.end_pos and chromosome == CNA_event.chromosome:
			if CNA_event.frac != 1:
				return "subclonal"
			elif CNA_event.major_allele == 1 and CNA_event.minor_allele == 0: # clonal single copy deletion
				return "deletion"
			elif CNA_event.major_allele == 1 and CNA_event.minor_allele == 1:
				return "neutral"
			else:
				return "clonal"
	
	return "unknown" # this means that we did not find any CNA event in the array CNV_array spanning given SNV


def SNV_consensus_copy_number_status(chromosome, SNV_position, consensus_CNV_array):
	   for CNV_event in consensus_CNV_array:
                  if SNV_position >= CNV_event.start_pos and SNV_position <= CNV_event.end_pos and chromosome == CNV_event.chromosome:
                          return "clonal"
 
           return "subclonal" # this means that the given SNV belongs to the region affected by some subclonal CNA and such regions are going to be discarded from CTPsingle's input
 


battenberg_CNV_file_location="SMRDO"
SNV_file_location=''

try:
	myopts, args = getopt.getopt(sys.argv[1:],"c:s:")

except getopt.GetoptError as e:
	print (str(e))
        print("Usage: %s -c battenberg_CNV_file_location -s SNV_file_location" % sys.argv[0])
        sys.exit(2)

for option, argument in myopts:
	if option == '-c':
        	battenberg_CNV_file_location=argument
	elif option == '-s':
		SNV_file_location=argument



############### Reading BATTENBERG FILE #####################

battenberg_CNA_segments	=	[];
batt_aberrant_length	=	0;
battenberg_CNV_file	=	open(battenberg_CNV_file_location, 'r');

battenberg_CNV_file.readline(); # read the header line
for line in battenberg_CNV_file:

	line_columns = line.split("\t");
	
	chromosome = line_columns[0];
	if chromosome == "X" or chromosome == "Y":
		continue;

	start_pos	= int(line_columns[1]);
	end_pos		= int(line_columns[2]);
	BAF		= float(line_columns[3]);
	ntot		= float(line_columns[6]);
	major		= int(line_columns[7]);
	minor		= int(line_columns[8]);
	frac		= float(line_columns[9]);
	#print "BATT: start {} end {} ntot {} major {} minor {} frac {}".format(start_pos, end_pos, ntot, major, minor, frac);
	
	if (minor == 1 and major == 1 and frac == 1) == False:
		batt_aberrant_length += end_pos - start_pos + 1;

	event = CNA();
	event.chromosome	=	chromosome;
	event.start_pos		=	start_pos;
	event.end_pos		=	end_pos;
	event.BAF		=	BAF;
	event.major_allele	=	major;	
	event.minor_allele	=	minor;
	event.frac		=	frac;
	
	battenberg_CNA_segments.append(event);

battenberg_CNV_file.close();



SNV_file		=	open(SNV_file_location, 'r');
output_frq_file		=	open("CTPsingle_input.frq", 'w');
output_frq_file.write("Chromosome Position Mutant Reference Mcount Rcount Multiplier Gender\n");
total_SNVs = 0;
copy_neutral_SNVs = 0;
used_SNVs = 0;

for line in SNV_file:
	if line[0] == "#":
		continue;

	total_SNVs += 1;
	line_columns = line.split("\t");
	SNV_chr = line_columns[0];
	SNV_pos = int(line_columns[1]);
    
	
	if SNV_chr == "X" or SNV_chr == "Y":
		continue;
        

	SNV_batt_CN_status = SNV_copy_number_status(SNV_chr, SNV_pos, battenberg_CNA_segments);

        if SNV_batt_CN_status == "subclonal":
		continue;
	if SNV_batt_CN_status == "deletion":
		continue;
	if SNV_batt_CN_status == "clonal":
		continue;
	if SNV_batt_CN_status != "neutral" and SNV_batt_CN_status != "unknown":
		print "Problem with battenberg status of SNV call"
	
	copy_neutral_SNVs += 1;
	multiplier = 2;



       	FILTER	= line_columns[6];
	if FILTER != "PASS":
		print "Filter different from PASS " + SNV_chr + " " + SNV_pos; 
		continue;
	
	INFO	= line_columns[7];
	INFO_entries = INFO.split(";");
	if INFO_entries[0] != "SOMATIC":
		continue; 



	FORMAT = line_columns[8];
	if FORMAT != "GT:AD:BQ:DP:FA:SS":
		print "FORMAT column not equal to GT:AD:BQ:DP:FA:SS";
		print SNV_file_location;
	
	AD_index	= -1;
	FORMAT_entries = FORMAT.split(":");
	for i in range(0, len(FORMAT_entries)):
		if FORMAT_entries[i] == "AD":
			AD_index = i;
	
	if AD_index == -1:
		print "Prolem. AD index not found" + SNV_chr + " " + SNV_pos;
		continue;

	normal		= line_columns[9];
	tumor		= line_columns[10];
	
	tumor_entries = tumor.split(":");
	tumor_AD_values = tumor_entries[AD_index];
	tumor_AD_values_entries = tumor_AD_values.split(",");
	tumor_reference_coverage = tumor_AD_values_entries[0];
	tumor_altered_coverage = tumor_AD_values_entries[1];        


	ref_nucleotide	= line_columns[3];
        alt_nucleotide	= line_columns[4];	
        
        #if tumor_total_coverage < 10*multiplier:
        #	continue;


        used_SNVs += 1;
        output_frq_file.write("{} {} {} {} {} {} {} {}\n".format(SNV_chr, SNV_pos, alt_nucleotide, ref_nucleotide, tumor_altered_coverage, tumor_reference_coverage, multiplier, gender));




output_log_file		=	open("Log_file.log", 'w');
battenberg_aberrant_percentage = float(100*batt_aberrant_length)/genome_length;
output_log_file.write("CNV_aberrant_percentage_Battenberg: \t{}\n".format(battenberg_aberrant_percentage));
output_log_file.write("Total_SNVs: \t{}\n".format(float(total_SNVs)));
output_log_file.write("CN_neutral_SNVs: \t{}\n".format(float(copy_neutral_SNVs)));
output_log_file.write("Used_SNVs:\t{}\n".format(float(used_SNVs)));
output_log_file.close();
